import React, { Component } from "react";
import { data } from "./dataGlasses";
import styles from "./glass.module.css";

export default class Glass extends Component {
  state = {
    dataGlass: data,
    model: {
      url: "",
      name: "",
      price: "",
      desc: "",
    },
  };

  handleGlass = (item) => {
    this.setState({
      model: {
        url: `${item.url}`,
        name: `${item.name}`,
        price: `${item.price}$`,
        desc: `${item.desc}`,
      },
    });
  };

  renderGlass = () => {
    return this.state.dataGlass.map((item, index) => {
      return (
        <img
          key={index}
          onClick={() => this.handleGlass(item)}
          style={{ width: `${100 / this.state.dataGlass.length}%` }}
          src={item.url}
          alt=""
        />
      );
    });
  };
  render() {
    return (
      <div
        className="container "
        style={{
          backgroundImage: "url(./glassesImage/background.jpg)",
          backgroundSize: "cover",
          width: "100",
          height: "100vh",
          backgroundRepeat: "no-repeat",
        }}
      >
        <div className="mx-auto" style={{ backgroundColor: "rgba(0,0,0,0.7)" }}>
          <h1
            className="text-center text-500 text-5xl"
            style={{ color: "white", marginBottom: "50px" }}
          >
            TRY GLASS APP ONLINE
          </h1>
        </div>
        <div>
          <div className={styles.glasses__card}>
            <div className={styles.glasses__avatar}>
              <img src="./glassesImage/model.jpg" alt="" width={300} />
            </div>
            <div className={styles.glasses}>
              <img src={this.state.model.url} alt="" width={180} />
            </div>
            <div
              className={styles.glasses__info}
              style={{ backgroundColor: "rgba(258,175,122,0.7)" }}
            >
              <p style={{ color: "blue" }}>
                {this.state.model.name} - {this.state.model.price}
              </p>
              <p style={{ color: "white" }}>{this.state.model.desc}</p>
            </div>
          </div>
        </div>
        <div
          className={styles.list__glasses}
          style={{ background: "rgba(0,0,0,0.7)" }}
        >
          {this.renderGlass()}
        </div>
      </div>
    );
  }
}
